$(document).ready(function()
{
	$(".fancy1").fancybox(
		{
            minWidth: "auto",
            margin: [0, 0, 0, 100], // top, right, bottom, left
		}
	);
    $(".fancy").fancybox();
	$(".gam").click(function()
	{
		var obj = $(".mobile_menu");
		if(obj.hasClass("open") == true)
		{
			obj.addClass("open").slideToggle(500);
			$(".header_menu .left").css({"z-index" : "4"});
		}
		else
		{
			obj.removeClass("open").slideToggle(500);
			setTimeout(function()
			{
				$(".header_menu .left").css({"z-index" : "3"});
			},500);
		}
	});
	
	$(".phone_icon").mouseenter(function(){$(this).children("a").children("span").animate({width:'toggle'}, 500)});
	$(".phone_icon").mouseleave(function(){$(this).children("a").children("span").animate({width:'toggle'}, 500)});
});