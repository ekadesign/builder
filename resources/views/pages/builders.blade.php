@extends('layouts.default')
@section('content')
    <body>
    <div class="gam news"></div>
@include('layouts.header__menu')
    <div class="news_block container">
        <div class="title"><h1>Выберите застройщика</h1></div>
        <div class="catalog">
            @forelse($builders as $builder)
                <div class="item">

                    <a href="{{route('builder', ['id' => $builder->id])}}"><div class="hover"></div></a>
                    <div class="items">
                        <div>
                            <div><p>Реализованных объектов</p></div>
                            <span><p>{{ $builder->realised_subjects ?? '0' }}</p></span></div>
                        <div>
                            <div><p>Готовых объектов</p></div>
                            <span><p>{{$builder->finished_subjects ?? '0' }}</p></span>
                        </div>
                    </div>
                    <div class="image"><img src="{{ asset('uploads/'.$builder->logo) }}"></div>
                    <div class="content"><img src="{{ asset('uploads/'.$builder->img) }}"></div>
                    <div class="title"><p>{{ $builder->name }}</p></div>
                </div>
            @empty
                <div class="row" style="margin: 15px">
                    <div class="h2 col-6 offset-5">Отсутствуют</div>
                </div>
            @endforelse
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="footer_menu">
                <ul>
                    <li><a href="{{route('index')}}">Главная</a></li>
                    <li><a href="{{route('builders')}}">Выберите застройщика</a></li>
                    <li><a href="{{route('map')}}">Карта застройки</a></li>
                    <li><a href="{{route('news')}}">Новости недвижимости</a></li>
                </ul>
            </div>
            <a href="{{route('index')}}">
            <div class="logo">
                <img src="/img/logo.png" />
            </div>
            </a>
            <div class="footer_phone">
                <p class="phone">8 (4922) 222-333</p>
                <p>получить консультацию о застройщике</p>
                <p><a class="fancy" href="#callback">Оставить заявку</a></p>
            </div>
        </div>
    </footer>
    <div class="popup" id="callback">
        <form action="">
            <p class="title">Заявка</p>
            <img class="form_back" src="/img/form_back.png" />
            <div class="form_row">
                <span>Имя</span>
                <input name="name" type="text" placeholder="Введите имя" />
            </div>
            <div class="form_row">
                <span>Телефон*</span>
                <input name="phone" type="text" placeholder="Введите номер телефона" />
            </div>
            <div class="form_row">
                <span>Email</span>
                <input name="email" type="text" placeholder="Введите ваш Email" />
            </div>
            <div class="form_row">
                <span>Сообщение</span>
                <input name="message" type="text" placeholder="Какой вопрос Вас интересует?" />
            </div>
            <div class="form_row submit">
                <input type="submit" value="Оставить заявку" />
            </div>
            <div class="form_row">
                <span>Нажимая на кнопку отправить вы даете свое согласие на обработку персональных данных и соглашаетесь с <a href="#">политикой конфиденциальности</a></span>
            </div>
        </form>
    </div>
    </body>
@endsection