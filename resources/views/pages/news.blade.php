@extends('layouts.default')
@section('content')
<body>
<div class="gam news"></div>
@include('layouts.header__menu')
<div class="news_block container">
    <div class="title"><h1>Новости недвижимости</h1></div>
    <div class="news">
        <div class="news_catalog">
        @forelse($news as $item)
        <div class="item" id="{{$item->id}}">
            <div class="date">{{date_format($item->created_at, 'Y.m.d')}}</div>
            <a href="#"><div class="image news__img"><img src="/uploads/{{$item->image}}"/></div></a>
            <a href="#"><div class="title">{{$item->name}}</div></a>
            <div class="description">{!!str_limit($item->description, 150)!!}</div>
        </div>
                @empty
                    <div class="row" style="margin: 15px">
                        <div class="h2 col-6 offset-5">Отсутствуют</div>
                    </div>
                @endforelse
            {{ $news->links() }}
            <input id="lastpage" type="hidden" value="{{$news->lastPage()}}">
            @if($news)
                <div class="more"><a id="showmore" href="{{$news->nextPageUrl()}}">Показать еще...</a></div>
            @endif
    </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="footer_menu">
            <ul>
                <li><a href="{{route('index')}}">Главная</a></li>
                <li><a href="{{route('builders')}}">Выбрать застройщика</a></li>
                <li><a href="{{route('map')}}">Карта застройки</a></li>
                <li><a href="{{route('news')}}">Новости недвижимости</a></li>
            </ul>
        </div>
        <a href="{{route('index')}}">
        <div class="logo">
            <img src="/img/logo.png" />
        </div>
        </a>
        <div class="footer_phone">
            <p class="phone">8 (4922) 222-333</p>
            <p>получить консультацию о застройщике</p>
            <p><a class="fancy" href="#callback">Оставить заявку</a></p>
        </div>
    </div>
</footer>
<script>
    window.onload = () =>
    {
        setInterval(() => {
        var items = document.getElementsByClassName('item');
        for (let i = 0; i < items.length; i++) {
            items[i].onclick = () => {
                axios.get('/api/news/' + items[i].id)
                    .then(function (response) {
                        let data = response.data;
                        $('#news > .title').text(data.name);
                        if(data.image) {
                            $('#news > .image').attr('src', '/uploads/' + data.image);
                        }
                        $('#news > .description').html(data.description);
                        $('#news > .tag').text(data.tag);
                        $('.open_news').trigger('click');

                    })

                    .catch(function (error) {
                        //console.log(error);
                    });
            }
        }
    }, 1000);
    }
</script>
</body>
@endsection