@extends('layouts.default')
@section('content')
	<body>
		<div class="gam"></div>
		<header>
			<div class="header__inner">
				<div class="header__inner--col header__inner--left">
					<div class="header__contacts">
						<div class="phone__row"><a class="phone__num fancy" href="#callback">8 (4922) 779-877</a></div>
						<div class="phone__row"><span class="phone__intro">получить бесплатную консультацию</span></div>
						<div class="phone__row"><a class="phone__num fancy" href="#callback">8 (800)301-11-19</a></div>
					</div>
					<div class="header__menu--left">
						<a class="menu__link" href="{{route('builders')}}">Выбрать застройщика</a>
						<a class="menu__link" href="{{route('map')}}">Карта застройщика</a>
						<a class="menu__link dop__link" href="{{route('news')}}">Новости недвижимости</a>
					</div>
				</div>
				<div class="header__inner--col header__inner--center"></div>
				<div class="header__inner--col header__inner--right">
					<div class="header__menu--right">
						<a class="menu__link right__link" href="{{route('news')}}">Новости недвижимости</a>
					</div>
					<a href="#action" class="header__banner fancy1 open_action1">Лучшие предложения<br>застройщиков!</a>
				</div>
			</div>
			<div class="header_menu">
				<div class="mobile_menu">
					<ul>
						<li><a href="#action" class="fancy1 open_action1">Лучшие предложения</a></li>
						<li><a href="{{route('index')}}">Главная</a></li>
						<li><a href="{{route('builders')}}">Выбрать застройщика</a></li>
						<li><a href="{{route('map')}}">Карта застройки</a></li>
						<li><a href="{{route('news')}}">Новости недвижимости</a></li>
					</ul>
				</div>
				<div class="header_block">
					<p class="title">Полный<br>справочник</p>
					<p class="description">по владимирской области</p>
					<div class="image"><img src="/img/logo.png" style="width: 50%;"></div>
				</div>
				<div class="stats">
					<div class="stats__inner">
						<div class="stats__col">
							<div class="stats__item">
								<div class="stats__item--inner">
									<div class="stats__pic" style="background-image: url('/img/h_icon1.png'); width:28px;height: 37px;"></div>
									<div class="stats__num">{{$cyfrals[0]['value'] ?? '0'}}</div>
									<div class="stats__name">{{$cyfrals[0]['translit']}}</div>
								</div>
							</div>
							<div class="stats__item">
								<div class="stats__item--inner">
									<div class="stats__pic" style="background-image: url('/img/h_icon2.png'); width: 44px; height: 37px;"></div>
									<div class="stats__num">{{$cyfrals[1]['value'] ?? '0'}}</div>
									<div class="stats__name">{{$cyfrals[1]['translit']}}</div>
								</div>
							</div>
						</div>
						<div class="stats__col">
							<div class="stats__item">
								<div class="stats__item--inner">
									<div class="stats__pic" style="background-image: url('/img/h_icon3.png'); width: 29px; height: 37px;"></div>
									<div class="stats__num">{{$cyfrals[2]['value'] ?? '0'}}</div>
									<div class="stats__name">{{$cyfrals[2]['translit']}}</div>
								</div>
							</div>
							<div class="stats__item">
								<div class="stats__item--inner">
									<div class="stats__pic" style="background-image: url('/img/h_icon4.png'); width: 39px; height: 37px;"></div>
									<div class="stats__num">{{$cyfrals[3]['value'] ?? '0'}}</div>
									<div class="stats__name">{{$cyfrals[3]['translit']}}</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div id="content">
			<div class="container">
				<div class="seo_block">
					<div class="title" id="builders"><h1>Выберите застройщика</h1></div>
					<div class="description">Купить квартиру, на самом деле, не так уж сложно. Сложно эту квартиру выбрать. Если вы уже решили, что вам нужна квартира в новостройке, стоит рассмотреть все варианты. На сайте представлена актуальная база нового жилья во Владимирие. В базе только проверенные надежные застройщики. Мы знаем о них все и заинтересованы выбрать для вас лучший вариант нового жилья во Владимире от застройщиков.</div>
				</div>

				<div class="catalog infinite-scroll">
					@forelse($builders as $builder)
					<div class="item">

						<a href="{{route('builder', ['id' => $builder->id])}}"><div class="hover"></div></a>
						<div class="items">
							<div>
								<div><p>Реализованных объектов</p></div>
								<span><p>{{ $builder->realised_subjects ?? '0' }}</p></span></div>
							<div>
								<div><p>Готовых объектов</p></div>
								<span><p>{{$builder->finished_subjects ?? '0' }}</p></span>
							</div>
						</div>
						<div class="image"><img src="{{ asset('uploads/'.$builder->logo) }}"></div>
						<div class="content"><img src="{{ asset('uploads/'.$builder->img) }}"></div>
						<div class="title"><p>{{ $builder->name }}</p></div>
					</div>
					@empty
						<div class="row" style="margin: 15px">
						<div class="h2 col-6 offset-5">Отсутствуют</div>
						</div>
					@endforelse

					{{ $builders->links() }}
						<input id="lastpage" type="hidden" value="{{$builders->lastPage()}}">
						@if($builders)
						<div class="more"><a id="showmore" href="{{$builders->nextPageUrl()}}">Показать еще...</a></div>
							@endif
				</div>
			</div>
		</div>
		<div class="map_filter">
			<div class="container">
				<p class="title" id="map">Карта застройки</p>
				<div class="filter">
					<div class="inner">
						<form name="searchform" method="get" id="search-form">
							<p>Выбор по параметрам:</p>
							<div class="filter_row">
								<div class="fancySelect__title">Район</div>
								<div>
									<select class="fancySelect" name="district">
										<option value=""></option>
										@foreach($districts as $district)
											<option value="{{$district->id}}">{{$district->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="filter_row">
								<div class="fancySelect__title">Застройщик</div>
								<div>
									<select class="fancySelect" name="builder">
										<option value=""></option>
										@foreach($buildersForMap as $builderForMap)
											<option value="{{$builderForMap->id}}">{{$builderForMap->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="filter_row">
								<div class="fancySelect__title">Жилой комплекс</div>
								<div>
									<select class="fancySelect" name="jk">
										<option value=""></option>
										@foreach($jks as $jk)
											<option value="{{$jk->id}}">{{$jk->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="btn desktop"><input type="submit" value="Поиск" /></div>
							<div class="btn mobile"><input type="submit" value="Поиск по объектам" /></div>
							<div class="btn"><input type="reset" value="Сбросить фильтр" /></div>
						</form>
					</div>
				</div>
				<div class="map_block" id="ymap">
				</div>
			</div>
		</div>
		<footer>
			<div class="container">
				<div class="footer_menu">
					<ul>
						<li><a href="{{route('index')}}">Главная</a></li>
						<li><a href="#builders">Выберите застройщика</a></li>
						<li><a href="#map">Карта застройки</a></li>
						<li><a href="{{route('news')}}">Новости недвижимости</a></li>
					</ul>
				</div>
				<a href="{{route('index')}}">
				<div class="logo">
					<img src="/img/logo.png" />
				</div>
				</a>
				<div class="footer_phone">
					<p class="phone"><span class="icon__phone">8 (4922) 222-333</span></p>
					<p>получить консультацию о застройщике</p>
					<p><a class="fancy" href="#callback">Оставить заявку</a></p>
				</div>
			</div>
		</footer>
	</body>
@endsection

@section('footer.js')
	<style type="text/css">
		#ymap {
			height: 520px;
		}
	</style>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<script>
		ymaps.ready(function() {
            var lat = 56.129042;
            var lng = 40.407030;
            var ymap = new ymaps.Map('ymap', {
                center: [lat, lng],
                zoom: 12
            }, {
                maxZoom: 15
            });

            var clusterIcons = [
				{
					href: '/img/map-marker.png',
					size: [50, 50],
					// Отступ, чтобы центр картинки совпадал с центром кластера.
					offset: [0, 0]
				},
				{
					href: '/img/map-marker.png',
					size: [80, 80],
					offset: [0, 0]
				}],
                // При размере кластера до 100 будет использована 1 картинка.
                // При размере кластера больше 100 будет использована 2 картинка.
                clusterNumbers = [100],
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FF0000; font-weight: bold;">$[properties.geoObjects.length]</div>');

            var clusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true,
                clusterHideIconOnBalloonOpen: false,
                geoObjectHideIconOnBalloonOpen: false,
                clusterBalloonContentLayout: 'cluster#balloonAccordion',
                clusterIcons: clusterIcons,
                clusterNumbers: clusterNumbers,
                clusterIconContentLayout: MyIconContentLayout
            });

            ymap.geoObjects.add(clusterer);

            var form = $('#search-form');
            form.on('submit', function(e) {
                e.preventDefault();
                filterMap();
			});

            var district = form.find('[name=district]');
            var builder = form.find('[name=builder]');
            var jk = form.find('[name=jk]');
            district.on('change', filterMap);
            builder.on('change', filterMap);
            jk.on('change', filterMap);

            form.find('[type=reset]').on('click', function() {
                builder.val(null);
                district.val(null);
                jk.val(null);
            });

            var timeout = null;

			function filterMap() {
			    if (timeout) {
			        clearTimeout(timeout);
				}

				timeout = setTimeout(function() {
				    timeout = null;
                    $.get('/api/objects/findbyparams?' + $.param({
                        district: district.val(),
                        builder: builder.val(),
                        jk: jk.val()
                    })).done(function(data) {
                        clusterer.removeAll();

                        for (var i = 0; i < data.length; i++) {
                            var obj = data[i];

                            var placemark = new ymaps.Placemark(obj.coords, {
                                hintContent: obj.text,
								balloonContent: obj.balloonContent,
								balloonContentHeader: obj.text,
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: '/img/map-marker.png',
                                iconImageSize: [30, 30],
                                iconImageOffset: [0, 0],
                                balloonPanelMaxMapArea: 0,
                                draggable: "true",
                                openEmptyBalloon: true
                            });

                            clusterer.add(placemark);
                        }
                    });
				}, 300);
			}

			filterMap();
		});
	</script>
@endsection