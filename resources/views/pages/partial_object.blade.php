<div class="item map-item">
    <a href="{{route('builder', ['id' => $item->builder->id])}}">
    <div class="hover"></div>
    </a>
    <div class="items">
        <div>
            <div><p>Реализованных объектов</p></div>
            <span><p>{{ $item->builder->realised_subjects ?? '0' }}</p></span></div>
        <div>
            <div><p>Готовых объектов</p></div>
            <span><p>{{$item->builder->finished_subjects ?? '0' }}</p></span>
        </div>
    </div>
    <div class="image"><img src="{{ asset('uploads/'.$item->builder->logo) }}"></div>
    <div class="content"><img src="{{ asset('uploads/'.$item->builder->img) }}"></div>
    <div class="title"><span>{{ $item->builder->name }}</span></div>
</div>