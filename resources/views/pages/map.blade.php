@extends('layouts.default')
@section('content')
	<body>
		<div class="gam"></div>
		@include('layouts.header__menu')
		<div class="map_filter">
			<div class="container">
				<p class="title" id="map">Карта застройки</p>
				<div class="filter">
					<div class="inner">
						<form name="searchform" method="get" id="search-form">
							<p>Выбор по параметрам:</p>
							<div class="filter_row">
								<div class="fancySelect__title">Район</div>
								<div>
									<select class="fancySelect" name="district">
										<option value=""></option>
										@foreach($districts as $district)
											<option value="{{$district->id}}">{{$district->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="filter_row">
								<div class="fancySelect__title">Застройщик</div>
								<div>
									<select class="fancySelect" name="builder">
										<option value=""></option>
										@foreach($buildersForMap as $builderForMap)
											<option value="{{$builderForMap->id}}">{{$builderForMap->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="filter_row">
								<div class="fancySelect__title">Жилой комплекс</div>
								<div>
									<select class="fancySelect" name="jk">
										<option value=""></option>
										@foreach($jks as $jk)
											<option value="{{$jk->id}}">{{$jk->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="btn desktop"><input type="submit" value="Поиск" /></div>
							<div class="btn mobile"><input type="submit" value="Поиск по объектам" /></div>
							<div class="btn"><input type="reset" value="Сбросить фильтр" /></div>
						</form>
					</div>
				</div>
				<div class="map_block" id="ymap">
				</div>
			</div>
		</div>
		<footer>
			<div class="container">
				<div class="footer_menu">
					<ul>
						<li><a href="{{route('index')}}">Главная</a></li>
						<li><a href="#builders">Выберите застройщика</a></li>
						<li><a href="#map">Карта застройки</a></li>
						<li><a href="{{route('news')}}">Новости недвижимости</a></li>
					</ul>
				</div>
				<a href="{{route('index')}}">
				<div class="logo">
					<img src="/img/logo.png" />
				</div>
				</a>
				<div class="footer_phone">
					<p class="phone">8 (4922) 222-333</p>
					<p>получить консультацию о застройщике</p>
					<p><a class="fancy" href="#callback">Оставить заявку</a></p>
				</div>
			</div>
		</footer>
	</body>
@endsection

@section('footer.js')
	<style type="text/css">
		#ymap {
			height: 520px;
		}
	</style>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<script>
		ymaps.ready(function() {
            var lat = 56.129042;
            var lng = 40.407030;
            var ymap = new ymaps.Map('ymap', {
                center: [lat, lng],
                zoom: 12
            }, {
                maxZoom: 15
            });

            var clusterIcons = [
				{
					href: '/img/map-marker.png',
					size: [50, 50],
					// Отступ, чтобы центр картинки совпадал с центром кластера.
					offset: [0, 0]
				},
				{
					href: '/img/map-marker.png',
					size: [80, 80],
					offset: [0, 0]
				}],
                // При размере кластера до 100 будет использована 1 картинка.
                // При размере кластера больше 100 будет использована 2 картинка.
                clusterNumbers = [100],
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FF0000; font-weight: bold;">$[properties.geoObjects.length]</div>');

            var clusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true,
                clusterHideIconOnBalloonOpen: false,
                geoObjectHideIconOnBalloonOpen: false,
                clusterBalloonContentLayout: 'cluster#balloonAccordion',
                clusterIcons: clusterIcons,
                clusterNumbers: clusterNumbers,
                clusterIconContentLayout: MyIconContentLayout
            });

            ymap.geoObjects.add(clusterer);

            var form = $('#search-form');
            form.on('submit', function(e) {
                e.preventDefault();
                filterMap();
			});

            var district = form.find('[name=district]');
            var builder = form.find('[name=builder]');
            var jk = form.find('[name=jk]');
            district.on('change', filterMap);
            builder.on('change', filterMap);
            jk.on('change', filterMap);

            form.find('[type=reset]').on('click', function() {
                builder.val(null);
                district.val(null);
                jk.val(null);
            });

            var timeout = null;

			function filterMap() {
			    if (timeout) {
			        clearTimeout(timeout);
				}

				timeout = setTimeout(function() {
				    timeout = null;
                    $.get('/api/objects/findbyparams?' + $.param({
                        district: district.val(),
                        builder: builder.val(),
                        jk: jk.val()
                    })).done(function(data) {
                        clusterer.removeAll();

                        for (var i = 0; i < data.length; i++) {
                            var obj = data[i];

                            var placemark = new ymaps.Placemark(obj.coords, {
                                hintContent: obj.text,
								balloonContent: obj.balloonContent,
								balloonContentHeader: obj.text,
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: '/img/map-marker.png',
                                iconImageSize: [30, 30],
                                iconImageOffset: [0, 0],
                                balloonPanelMaxMapArea: 0,
                                draggable: "true",
                                openEmptyBalloon: true
                            });

                            clusterer.add(placemark);
                        }
                    });
				}, 300);
			}

			filterMap();
		});
	</script>
@endsection