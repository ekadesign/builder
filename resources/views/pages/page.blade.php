@extends('layouts.default')
@section('content')
    <body>
    <div class="gam news"></div>
    @include('layouts.header__menu')

    <div class="objects">
        <div class="container">
            <p class="content_title">Активные объекты</p>
            <div class="objects_catalog objects_catalog_pc infinite-scroll">
                @forelse($activeObjects as $object)
                    <div class="item">

                        <div class="date"><b>Срок
                                сдачи:</b> {{$object->release_date->release_date ?? ''}} {{$object->release_year}}</div>
                        <div class="content">
                            <a href="#"><img class="card_img" src="/uploads/{{$object->logo}}"/></a>
                            <div class="phone_icon">
                                <a href="#callback" class="fancy"><img
                                            src="/img/phone2.png"/><span> Оставить заявку</span></a>
                            </div>
                        </div>
                        <div class="title"><a href="#"><span>{{$object->jk->name ?? ''}}</span></a></div>
                        <div class="description">
                            <p><b>Адрес: </b>{{$object->address}}</p>
                            <p><b>Стены: </b>{{$object->walls}}</p>
                            <p><b>Этажность: </b>{{$object->level}}</p>
                            <p><b>Отопление: </b>{{$object->heat}}</p>
                            <p><b>Отделка квартир: </b>{{$object->finish}}</p>
                        </div>
                        <div class="phone"><span>8 (4222) 377-387</span></div>
                        <div class="site"><span><a href="{{$object->link}}">{{$object->link}}</a></span></div>
                    </div>
                @empty
                    <p>Отсутствуют</p>
                @endforelse
                {{ $activeObjects->links() }}
                <input id="lastpage" type="hidden" value="{{$activeObjects->lastPage()}}">
                @if($activeObjects)
                    <div class="more"><a id="showmore" href="{{$activeObjects->nextPageUrl()}}">Показать еще...</a>
                    </div>
                @endif
            </div>
            <div class="objects_catalog mobile">
                <div class="item">
                    <div class="left">
                        <img class="card_img" src="/img/obj1.jpg" style="width:100%;"/>
                        </a>
                        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.
                        </div>
                        <div class="site"><span><a href="http://moskovskiy33.ru">moskovskiy33.ru</a></span></div>
                    </div>
                    <div class="right">
                        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
                        <div class="description">
                            <p><b>Адрес:</b>ул. Ставровская к.2</p>
                            <p><b>Стены:</b>монолитные</p>
                            <p><b>Этажность:</b>18 этажей</p>
                            <p><b>Отопление:</b>крышная котельная</p>
                            <p><b>Отделка квартир:</b>строительная</p>
                        </div>
                        <div class="phone"><span>8 (4222) 377-387</span>
                            <div class="phone_icon"><a href="#callback" class="fancy"><img src="/img/phone2.png"/></div>
                            </a></div>
                    </div>
                </div>
                <div class="item">
                    <div class="left">
                        <img class="card_img" src="/img/obj1.jpg" style="width:100%;"/>
                        </a>
                        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.
                        </div>
                        <div class="site"><span><a href="http://moskovskiy33.ru">moskovskiy33.ru</a></span></div>
                    </div>
                    <div class="right">
                        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
                        <div class="description">
                            <p><b>Адрес:</b>ул. Ставровская к.2</p>
                            <p><b>Стены:</b>монолитные</p>
                            <p><b>Этажность:</b>18 этажей</p>
                            <p><b>Отопление:</b>крышная котельная</p>
                            <p><b>Отделка квартир:</b>строительная</p>
                        </div>
                        <div class="phone"><span>8 (4222) 377-387</span>
                            <div class="phone_icon"><a href="#callback" class="fancy"><img src="/img/phone2.png"/></div>
                            </a></div>
                    </div>
                </div>
                <div class="item">
                    <div class="left">
                        <img class="card_img" src="/img/obj1.jpg" style="width:100%;"/>
                        </a>
                        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.
                        </div>
                        <div class="site"><span><a href="http://moskovskiy33.ru">moskovskiy33.ru</a></span></div>
                    </div>
                    <div class="right">
                        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
                        <div class="description">
                            <p><b>Адрес:</b>ул. Ставровская к.2</p>
                            <p><b>Стены:</b>монолитные</p>
                            <p><b>Этажность:</b>18 этажей</p>
                            <p><b>Отопление:</b>крышная котельная</p>
                            <p><b>Отделка квартир:</b>строительная</p>
                        </div>
                        <div class="phone"><span>8 (4222) 377-387</span>
                            <div class="phone_icon"><a href="#callback" class="fancy"><img src="/img/phone2.png"/></div>
                            </a></div>
                    </div>
                </div>
                <div class="item">
                    <div class="left">
                        <img class="card_img" src="/img/obj1.jpg" style="width:100%;"/>
                        </a>
                        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.
                        </div>
                        <div class="site"><span><a href="http://moskovskiy33.ru">moskovskiy33.ru</a></span></div>
                    </div>
                    <div class="right">
                        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
                        <div class="description">
                            <p><b>Адрес:</b>ул. Ставровская к.2</p>
                            <p><b>Стены:</b>монолитные</p>
                            <p><b>Этажность:</b>18 этажей</p>
                            <p><b>Отопление:</b>крышная котельная</p>
                            <p><b>Отделка квартир:</b>строительная</p>
                        </div>
                        <div class="phone"><span>8 (4222) 377-387</span>
                            <div class="phone_icon"><a href="#callback" class="fancy"><img src="/img/phone2.png"/></div>
                            </a></div>
                    </div>
                </div>
                <div class="item">
                    <div class="left">
                        <img class="card_img" src="/img/obj1.jpg" style="width:100%;"/>
                        </a>
                        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.
                        </div>
                        <div class="site"><span><a href="http://moskovskiy33.ru">moskovskiy33.ru</a></span></div>
                    </div>
                    <div class="right">
                        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
                        <div class="description">
                            <p><b>Адрес:</b>ул. Ставровская к.2</p>
                            <p><b>Стены:</b>монолитные</p>
                            <p><b>Этажность:</b>18 этажей</p>
                            <p><b>Отопление:</b>крышная котельная</p>
                            <p><b>Отделка квартир:</b>строительная</p>
                        </div>
                        <div class="phone"><span>8 (4222) 377-387</span>
                            <div class="phone_icon"><a href="#callback" class="fancy"><img src="/img/phone2.png"/></div>
                            </a></div>
                    </div>
                </div>
                <div class="item">
                    <div class="left">
                        <img class="card_img" src="/img/obj1.jpg" style="width:100%;"/>
                        </a>
                        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.
                        </div>
                        <div class="site"><span><a href="http://moskovskiy33.ru">moskovskiy33.ru</a></span></div>
                    </div>
                    <div class="right">
                        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
                        <div class="description">
                            <p><b>Адрес:</b>ул. Ставровская к.2</p>
                            <p><b>Стены:</b>монолитные</p>
                            <p><b>Этажность:</b>18 этажей</p>
                            <p><b>Отопление:</b>крышная котельная</p>
                            <p><b>Отделка квартир:</b>строительная</p>
                        </div>
                        <div class="phone"><span>8 (4222) 377-387</span>
                            <div class="phone_icon"><a href="#callback" class="fancy"><img src="/img/phone2.png"/></div>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="objects nobg">
        <div class="container">
            <p class="content_title">Реализованные объекты
            <p>
                <div class="objects_catalog">
                    <div class="item">
                        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.</div>
                        <div class="content"><a href="#"><img class="card_img" src="/img/obj1.jpg"></a></div>
                        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
                        <div class="description">
            <p><b>Адрес:</b>ул. Ставровская к.2</p>
            <p><b>Стены:</b>монолитные</p>
            <p><b>Этажность:</b>18 этажей</p>
            <p><b>Отопление:</b>крышная котельная</p>
            <p><b>Отделка квартир:</b>строительная</p>
        </div>
    </div>
    <div class="item">
        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.</div>
        <div class="content"><a href="#"><img class="card_img" src="/img/obj1.jpg"></a></div>
        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
        <div class="description">
            <p><b>Адрес:</b>ул. Ставровская к.2</p>
            <p><b>Стены:</b>монолитные</p>
            <p><b>Этажность:</b>18 этажей</p>
            <p><b>Отопление:</b>крышная котельная</p>
            <p><b>Отделка квартир:</b>строительная</p>
        </div>
    </div>
    <div class="item">
        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.</div>
        <div class="content"><a href="#"><img class="card_img" src="/img/obj1.jpg"></a></div>
        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
        <div class="description">
            <p><b>Адрес:</b>ул. Ставровская к.2</p>
            <p><b>Стены:</b>монолитные</p>
            <p><b>Этажность:</b>18 этажей</p>
            <p><b>Отопление:</b>крышная котельная</p>
            <p><b>Отделка квартир:</b>строительная</p>
        </div>
    </div>
    <div class="item">
        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.</div>
        <div class="content"><a href="#"><img class="card_img" src="/img/obj1.jpg"></a></div>
        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
        <div class="description">
            <p><b>Адрес:</b>ул. Ставровская к.2</p>
            <p><b>Стены:</b>монолитные</p>
            <p><b>Этажность:</b>18 этажей</p>
            <p><b>Отопление:</b>крышная котельная</p>
            <p><b>Отделка квартир:</b>строительная</p>
        </div>
    </div>
    <div class="item">
        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.</div>
        <div class="content"><a href="#"><img class="card_img" src="/img/obj1.jpg"></a></div>
        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
        <div class="description">
            <p><b>Адрес:</b>ул. Ставровская к.2</p>
            <p><b>Стены:</b>монолитные</p>
            <p><b>Этажность:</b>18 этажей</p>
            <p><b>Отопление:</b>крышная котельная</p>
            <p><b>Отделка квартир:</b>строительная</p>
        </div>
    </div>
    <div class="item">
        <div class="date"><b>Срок сдачи:</b> 4 квартал 2019г.</div>
        <div class="content"><a href="#"><img class="card_img" src="/img/obj1.jpg"></a></div>
        <div class="title"><a href="#"><span>жк "московский"</span></a></div>
        <div class="description">
            <p><b>Адрес:</b>ул. Ставровская к.2</p>
            <p><b>Стены:</b>монолитные</p>
            <p><b>Этажность:</b>18 этажей</p>
            <p><b>Отопление:</b>крышная котельная</p>
            <p><b>Отделка квартир:</b>строительная</p>
        </div>
    </div>
    </div>
    <div class="more"><a href="#">Показать еще...</a></div>
    </div>
    </div>
    <div class="content_text container">
        <p><b>Юридическое наименование:</b> ООО «Новый мир плюс»</p>
        <p><b>Дата создания:</b> 1992 г.</p>
        <p><b>Генеральный директор:</b> Перцов Василий Николаевич</p>
        <p><b>Лицензия на ведение строительной деятельности:</b> №д597955 от 15.08.2005г.
            ГС-1-33-02-27-0-3328423475-002361-2</p>
        <p></p>
        <p>
            <b>Компания «Новый мир плюс»</b>
            - один из самых надежных застройщиков города Владимира. Компания работает в сфере строительства почти
            четверть века,
            на ее счету тысячи квадратных метров отстроенной недвижимости - жилой и коммерческой. Отличительный стиль
            работы
            ООО «Новый мир плюс» -стремление следовать всем современным технологиям в строительстве и четкое соблюдение
            сроков сдачи объектов.
        </p>
        <p><b>Руководство компании</b><br>
            Строительная компания «Новый мир плюс» начала работу в 1992 году (в то время фирма носила другое название).
            С момента основания
            организацией руководит Василий Николаевич Перцов. В настоящее время Василий Николаевич занимает должность
            генерального директора.
            В.Н.Перцов также входит в Совет Народных депутатов г.Владимира от фракции «Единая Россия» и ведет прием
            граждан в микрорайонах
            Энергетик и Юрьевец. В качестве депутата Василий Николаевич принимает участие в работе комитетов:<br>
        <ul>
            <li>по градостроительству, архитектуре, земельным отношениям,</li>
            <li>по экономической политике, имущественному комплексу, развитию</li>
            <li>предпринимательства и потребительского рынка.</li>
        </ul>
        </p>
        <p><b>Направления деятельности</b><br>
            Кроме основного вида работ — строительства жилья, компания «Новый мирплюс» развивает и другие
            направления:<br>
        <ul>
            <li>управление проектами в строительной сфере,</li>
            <li>консультационная, инженерная, инвестиционная, имущественно-правовая, оценочная деятельность,</li>
            <li>участие в реконструкции зданий,</li>
            <li>создание современных строительных технологий,</li>
            <li>разработка эффективных стратегий управления в области строительства.</li>
        </ul>
        </p>
        <div class="left">
            <p><b>Сданные объекты</b><br>
                Эти современные офисы и комфортабельные дома уже возведены в городе Владимире:<br>
                1) Офисное здание по ул.Мусоргского, 1-а.<br>
                2) Жилые дома:<br>
            <ul>
                <li>ул. Студенческая (корпус 3), 16-д,</li>
                <li>ул. Студенческая, 16-б,</li>
                <li>ул. Суздальская, 5-б,</li>
                <li>ул. Красная,</li>
                <li>ул. Пугачева,</li>
                <li>ул. Михайловская, 59-а,</li>
                <li>проспект Строителей, 42-д,</li>
                <li>ул.(или пр-т) Строителей,</li>
                <li>ул. Тракторная, 1-г,</li>
                <li>комплекс многоквартирных домов по ул.Сурикова, 10-а и 10-б,</li>
                <li>ул. Михалькова,</li>
                <li>комплекс многоквартирных жилых домов на ул. Нижняя Дуброва.</li>
            </ul>
            </p>
        </div>
        <div class="right">
            <p><b>Строящееся жилье</b><br>
                В скором времени жильцы получат ключи от квартир в новостройках:<br>
            <ul>
                <li>три многоквартирных дома, возводимые комплексом, со встроенным детсадом и торгово-офисными
                    помещениями на ул.Н.Дуброва, 1-я очередь,
                </li>
                <li>ЖК «Московский» на ул.Ставровской,</li>
                <li>ЖК «Парк Университет» на улице Мира,</li>
                <li>ЖК (2 многоквартирных дома) на Нижней Дуброве, 2-я очередь.</li>
            </ul>
            </p>
            <p>
                Цены на строящееся жилье можно уточнить в отделе продаж компании «Новый мир плюс».<br>
                Застройщик, Новый мир плюс, город Владимир, цена, новостройка, ООО, отдел, продажа, квартира, дом,
                жилье, недвижимость, строительная компания.
            </p>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="footer_menu">
                <ul>
                    <li><a href="{{route('index')}}">Главная</a></li>
                    <li><a href="{{route('builders')}}">Выберите застройщика</a></li>
                    <li><a href="{{route('map')}}">Карта застройки</a></li>
                    <li><a href="{{route('news')}}">Новости недвижимости</a></li>
                </ul>
            </div>
            <div class="logo">
                <a href="{{route('index')}}"><img src="/img/logo.png"/></a>
            </div>
            <div class="footer_phone">
                <i class="fa fa-phone"></i><span class="phone">8 (4922) 222-333</span>
                <p>получить консультацию о застройщике</p>
                <p><a class="fancy" href="#callback">Оставить заявку</a></p>
            </div>
        </div>
    </footer>
    <div class="popup" id="callback">
        <form action="">
            <p class="title">Заявка</p>
            <img class="form_back" src="/img/form_back.png"/>
            <div class="form_row">
                <span>Имя</span>
                <input name="name" type="text" placeholder="Введите имя"/>
            </div>
            <div class="form_row">
                <span>Телефон*</span>
                <input name="phone" type="text" placeholder="Введите номер телефона"/>
            </div>
            <div class="form_row">
                <span>Email</span>
                <input name="email" type="text" placeholder="Введите ваш Email"/>
            </div>
            <div class="form_row">
                <span>Сообщение</span>
                <input name="message" type="text" placeholder="Какой вопрос Вас интересует?"/>
            </div>
            <div class="form_row submit">
                <input type="submit" value="Оставить заявку"/>
            </div>
            <div class="form_row">
                <span>Нажимая на кнопку отправить вы даете свое согласие на обработку персональных данных и соглашаетесь с <a
                            href="#">политикой конфиденциальности</a></span>
            </div>
        </form>
    </div>
    </body>
@endsection