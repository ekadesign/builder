@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        @include('backpack::inc.sidebar_user_panel')

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          {{-- <li class="header">{{ trans('backpack::base.administration') }}</li> --}}
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
          <li><a href="{{ backpack_url('builder') }}"><i class="fa fa-dashboard"></i> <span>Застройщики</span></a></li>
          <li><a href="{{ backpack_url('subject') }}"><i class="fa fa-dashboard"></i> <span>Объекты</span></a></li>
          <li><a href="{{ backpack_url('cyfrals') }}"><i class="fa fa-dashboard"></i> <span>Значки на главной</span></a></li>
          <li><a href="{{ backpack_url('district') }}"><i class="fa fa-dashboard"></i> <span>Районы</span></a></li>
          <li><a href="{{ backpack_url('jk') }}"><i class="fa fa-dashboard"></i> <span>Жк</span></a></li>
          <li><a href="{{ backpack_url('news') }}"><i class="fa fa-dashboard"></i> <span>Новости</span></a></li>
            <li><a href="{{ backpack_url('slides') }}"><i class="fa fa-dashboard"></i> <span>Слайды</span></a></li>

          <!-- ======================================= -->
          {{-- <li class="header">Other menus</li> --}}
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
