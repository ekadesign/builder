<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<html>
	<head>
		<title> </title>
		<meta charset="utf-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width">
		<link href="/css/jquery.fancybox.css" media="all" rel="stylesheet" type="text/css">
		<link type="text/css" rel="stylesheet" href="/css/style.css" />
		<link type="text/css" rel="stylesheet" href="/css/jquery.fancybox.css" />
		<link type="text/css" rel="stylesheet" href="/css/slick.css" />
		<link type="text/css" rel="stylesheet" href="/css/slick-theme.css" />
		<link type="text/css" rel="stylesheet" href="/fonts/stylesheet.css" />
		<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	</head>