<div class="popup" id="callback">
    <form action="">
        <p class="title">Заявка</p>
        <img class="form_back" src="/img/form_back.png" />
        <div class="form_row">
            <span>Имя</span>
            <input name="name" type="text" placeholder="Введите имя" />
        </div>
        <div class="form_row">
            <span>Телефон*</span>
            <input name="phone" type="text" placeholder="Введите номер телефона" />
        </div>
        <div class="form_row">
            <span>Email</span>
            <input name="email" type="text" placeholder="Введите ваш Email" />
        </div>
        <div class="form_row">
            <span>Сообщение</span>
            <input name="message" type="text" placeholder="Какой вопрос Вас интересует?" />
        </div>
        <div class="form_row submit">
            <input type="submit" value="Оставить заявку" />
        </div>
        <div class="form_row">
            <span>Нажимая на кнопку отправить вы даете свое согласие на обработку персональных данных и соглашаетесь с <a href="#">политикой конфиденциальности</a></span>
        </div>
    </form>
</div>
<a href="#action" class="fancy1 open_action" data-fancybox>qweqwe</a>
<div class="popup popaction" id="action">
    @if ($slides->count())
        <div id="slider">
            <ul>
                @foreach ($slides as $slide)
                    <li>
                        <a href="{{ $slide->link }}">
                            <img src="/{{ $slide->image }}" alt="">
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
<a href="#news" class="fancy open_news"></a>
<div class="popup" id="news">
    <p class="title"></p>
    <img src="" class="image img-responsive float-left">
    <p class="description"></p>
    <p class="tag"></p>
</div>
<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('/js/app.js') }}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/js/slick.js"></script>
<script src="/js/jquery.fancybox.js"></script>
<script src="/js/jscroll.js"></script>
<script src="/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $('document').ready(function(){
        //add endless scroll
		$('[data-fancybox]').fancybox({
			autoFocus : false,
			afterShow: function(){
				$('#slider ul').slick({
					infinite: false,
					dots:true,
					slidesToShow: 1,
					slidesToScroll: 1,
					fade: true,
				});
			},
			afterClose: function(){
				$('.popup_slider').slick('unslick');
			}
		});

		$('.fancySelect').select2();
        $('ul.pagination').hide();
        var counter = 2;
        var lastPage = $('#lastpage').val();
        if(lastPage == '1') {
            $('#showmore').remove();
        }
        $(function() {
            $('.catalog').jscroll({
                autoTrigger: false,
                loadingHtml: '<img class="center-block" src="https://demos.laraget.com/images/loading.gif" alt="Загрузка..." />',
                padding: 0,
                nextSelector: '#showmore',
                contentSelector: 'div.catalog',
                callback: function() {
                    if(counter == lastPage) {
                        $('#showmore').remove();
                    }
                    counter ++;
                    $('ul.pagination').remove();
                }
            });
        });
        //end endless scroll

        //add endless scroll
        $('ul.pagination').hide();
        var counter = 2;
        var lastPage = $('#lastpage').val();
        if(lastPage == '1') {
            $('#showmore').remove();
        }
        $(function() {
            $('.objects_catalog_pc').jscroll({
                autoTrigger: false,
                loadingHtml: '<img class="center-block" src="https://demos.laraget.com/images/loading.gif" alt="Загрузка..." />',
                padding: 0,
                nextSelector: '#showmore',
                contentSelector: 'div.objects_catalog_pc',
                callback: function() {
                    if(counter == lastPage) {
                        $('#showmore').remove();
                    }
                    counter ++;
                    $('ul.pagination').remove();
                }
            });
        });
        //end endless scroll


        //add endless scroll
        $('ul.pagination').hide();
        var counter = 2;
        var lastPage = $('#lastpage').val();
        if(lastPage == '1') {
            $('#showmore').remove();
        }
        $(function() {
            $('.news_catalog').jscroll({
                autoTrigger: false,
                loadingHtml: '<img class="center-block" src="https://demos.laraget.com/images/loading.gif" alt="Загрузка..." />',
                padding: 0,
                nextSelector: '#showmore',
                contentSelector: 'div.news_catalog',
                callback: function() {
                    if(counter == lastPage) {
                        $('#showmore').remove();
                    }
                    counter ++;
                    $('ul.pagination').remove();
                }
            });
        });
        //end endless scroll
    });
</script>
@yield('footer.js')
</html>