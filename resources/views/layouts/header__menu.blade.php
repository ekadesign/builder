<header class="cert">
    <div class="header__inner">
        <div class="header__inner--col header__inner--left">
            <div class="header__contacts">
                <div class="phone__row"><a class="phone__num" href="tel:84922779877">8 (4922) 779-877</a></div>
                <div class="phone__row"><span class="phone__intro">получить бесплатную консультацию</span></div>
                <div class="phone__row"><a class="phone__num" href="tel:88003011119">8 (800)301-11-19</a></div>
            </div>
            <div class="header__menu--left">
                <a class="menu__link" href="{{route('builders')}}">Выбрать застройщика</a>
                <a class="menu__link" href="{{route('map')}}">Карта застройщика</a>
                <a class="menu__link dop__link" href="{{route('news')}}">Новости недвижимости</a>
            </div>
        </div>
        <div class="header__inner--col header__inner--center">
            <div class="inner__logo">
                <a href="{{route('index')}}">
                    <div class="logo__pic">
                        <img src="/img/logo__inner.png" alt="">
                    </div>
                </a>
            </div>
        </div>
        <div class="header__inner--col header__inner--right">
            <div class="header__menu--right">
                <a class="menu__link right__link" href="{{route('news')}}">Новости недвижимости</a>
            </div>
            <a href="#action" class="header__banner fancy1 open_action1">Лучшие предложения<br>застройщиков!</a>
        </div>
    </div>

    @if(Request::is('builders/*'))
        <div class="container">
            <div class="cert_block">
                <div class="left">
                    @if($builder->certification)
                        @foreach($builder->certification as $indexKey => $item)
                            @if($indexKey % 2 == 0)
                                <div class="cert"><a href="/uploads/{{$item}}" class="fancy"><img
                                                src="/uploads/{{$item}}"/></a></div>
                            @endif
                        @endforeach
                    @endif
                </div>
                <div class="center">
                    <div class="title">
                        @if($builder->logo)
                            <img src="/uploads/{{ $builder->logo }}"/>
                        @endif
                        <h1>о строительной компании {{$builder->name}}</h1>
                    </div>
                    <div class="text">
                        <div id="body">
                            {!! $builder->body !!}
                        </div>
                        <p><a href="#callback" class="btn fancy">Уточнить подробности у специалиста</a></p>
                    </div>
                </div>
                <div class="right">
                    @if($builder->certification)
                        @foreach($builder->certification as $indexKey => $item)
                            @if($indexKey % 2 == 1)
                                <div class="cert"><a href="/uploads/{{$item}}" class="fancy"><img
                                                src="/uploads/{{$item}}"/></a></div>
                            @endif
                        @endforeach
                    @endif
                </div>

            </div>
        </div>
    @endif

</header>