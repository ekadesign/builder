<?php
/**
 * Created by PhpStorm.
 * User: mrShes
 * Date: 06.03.2018
 * Time: 21:43
 */

Route::group([], function()
{
    CRUD::resource('builder', 'BuilderCrudController');
    CRUD::resource('subject', 'SubjectCrudController');
    CRUD::resource('cyfrals', 'CyfralsCrudController');
    CRUD::resource('district', 'DistrictCrudController');
    CRUD::resource('news', 'NewsCrudController');
    CRUD::resource('jk', 'JkCrudController');
    CRUD::resource('slides', 'SlidesCrudController');
});