<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/news', 'HomeController@news')->name('news');

Route::get('/', 'HomeController@index')->name('index');

Route::get('/builders', 'HomeController@builders')->name('builders');

Route::get('/map', 'HomeController@map')->name('map');

Route::get('/best', 'HomeController@best')->name('best');


Route::group(['prefix' => 'builders'], function () {
    Route::get('/{id}', 'HomeController@builderget')->name('builder');
});


