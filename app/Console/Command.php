<?php
/**
 * Created by PhpStorm.
 * User: warme
 * Date: 04.03.2018
 * Time: 20:39
 */

namespace App\Console;


class Command extends \Illuminate\Console\Command
{

    /**
     * Execute the console command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return mixed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $method = method_exists($this, 'handle') ? 'handle' : 'fire';
        return $this->laravel->call([$this, $method]);
    }

}