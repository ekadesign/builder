<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'nullable',
            'name' => 'required',
            'release_date_id' => 'required',
            'address' => 'required',
            'walls' => 'required',
            'level' => 'required',
            'link' => 'nullable',
            'heat' => 'required',
            'finish' => 'nullable',
            'body' => 'required',
            'builder_id' => 'required',
            'builded' => 'required',
            'jk_id' => 'required|numeric',
            'district_id' => 'required|numeric',
            'coordinates' => 'required',
            'release_year' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
