<?php

namespace App\Http\Controllers\Api;

use App\Models\News;
use App\Models\Subject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    public function index(){
        return response()->json(News::all());
    }

    public function findById($id){
        return response()->json(News::find($id));
    }
}
