<?php

namespace App\Http\Controllers\Api;

use App\Models\Subject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class ObjectController extends Controller
{
    public function index()
    {
        return response()->json(Subject::all());
    }

    public function findByParams(Request $request)
    {
        $objects = Subject::query();

        if (!empty($request->district)) {
            $objects->where('district_id', $request->district);
        }
        if (!empty($request->builder)) {
            $objects->where('builder_id', $request->builder);
        }
        if (!empty($request->jk)) {
            $objects->where('jk_id', $request->jk);
        }

        $res = $objects->get();

        $response = [];

        foreach ($res as $item) {
            $response [] = [
                'coords' => explode(',', $item->coordinates),
                'text' => $item->name,
                'balloonContent' => (string)View::make('pages.partial_object', ['item' => $item])
            ];
        }

        return response()->json($response);
    }
}
