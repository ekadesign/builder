<?php

namespace App\Http\Controllers;

use App\Models\Cyfral;
use App\Models\Builder;
use App\Models\District;
use App\Models\Jk;
use App\Models\News;
use App\Models\Slide;
use App\Models\Subject;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $builders = Builder::paginate(6);
        $buildersForMap = Builder::all();
        $districts = District::all();
        $jks = Jk::all();
        $cyfrals = Cyfral::all()->toArray();
        $slides = Slide::where('active', true)->orderBy('order')->get();

        return view('pages.index',compact('builders', 'buildersForMap', 'cyfrals', 'districts', 'jks', 'slides'));
    }

    public function map()
    {
        $builders = Builder::paginate(6);
        $buildersForMap = Builder::all();
        $districts = District::all();
        $jks = Jk::all();
        $cyfrals = Cyfral::all()->toArray();
        $slides = Slide::where('active', true)->orderBy('order')->get();

        return view('pages.map',compact('builders', 'buildersForMap', 'cyfrals', 'districts', 'jks', 'slides'));
    }

    public function news()
    {
        $slides = Slide::where('active', true)->orderBy('order')->get();
        $news = News::paginate(6);
        return view('pages.news', compact('news', 'slides'));
    }


    public function best(){
        $builders = Builder::paginate(6);
        $slides = Slide::where('active', true)->orderBy('order')->get();
        $subjects = Subject::where('builded', 1);
        return view('pages.builders', compact('builders','subjects', 'slides'));
    }

    public function builders(){
        $slides = Slide::where('active', true)->orderBy('order')->get();
        $builders = Builder::all();
        return view('pages.builders', compact('builders', 'slides'));
    }

    public function builderGet($id)
    {
        $slides = Slide::where('active', true)->orderBy('order')->get();
        $builder = Builder::find($id);
        $activeObjects = Subject::where('builder_id', $id)->paginate(6);
        return view('pages.page', compact('builder', 'activeObjects', 'slides'));
    }
}
