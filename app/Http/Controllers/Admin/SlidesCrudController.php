<?php

namespace App\Http\Controllers\Admin;

Use App\Models\Jk;
use App\Models\Builder;
use App\Models\District;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SlideRequest as StoreRequest;
use App\Http\Requests\SlideRequest as UpdateRequest;

class SlidesCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Slide');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/slides');
        $this->crud->setEntityNameStrings('Слайд', 'Слайды');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->setColumns([
            [
                'name' => 'link', // it works in Tinker: $p->type1->field1
                'label' => 'Ссылка',
                'type' => 'text',
            ],
            [
                'name' => 'order', // it works in Tinker: $p->type1->field1
                'label' => 'Порядок',
                'type' => 'text',
            ],
            [
                'name' => 'active', // it works in Tinker: $p->type1->field1
                'label' => 'Активно',
                'type' => 'checkbox',
            ],
            [
                'label' => "Картинка", // Table column heading
                'type' => "image",
                'name' => 'image', // the column that contains the ID of that connected entity;
            ]
        ]);

        // ------ CRUD FIELDS
        $this->crud->addFields([
            [
                'name' => 'link',
                'label' => 'Ссылка',
                'type' => 'text',
            ],
        ]);

        $this->crud->addField([
            'name' => 'image',
            'label' => 'Картинка',
            'type' => 'image',
            'upload' => true,
        ]);

        $this->crud->addField([
            'name' => 'order',
            'label' => 'Порядок',
            'type' => 'text',
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'active',
            'label' => 'Активно',
            'type' => 'checkbox',
            'default' => true
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
