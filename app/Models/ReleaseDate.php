<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReleaseDate extends Model
{
    protected $table = 'release_dates';

    protected $fillable = ['release_date'];

    protected function subjects()
    {
        return $this->belongsToMany(Subject::class);
}
}
