<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Jk extends Model
{
    use CrudTrait;

    protected $table = 'jks';

    protected $fillable = ['name', 'builder_id', 'district_id'];

    public function builder(){
        return $this->belongsTo('App\Models\Builder', 'builder_id', 'id');
    }
}
