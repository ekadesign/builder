<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Slide extends Model
{
    use CrudTrait;

    protected $table = 'slides';

    protected $fillable = ['link', 'image', 'order', 'active'];

    /**
     * @param $value
     */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $destination_path = "img";
        if ($value == null) {
            File::delete(public_path($destination_path . '/' . $this->{$attribute_name}));
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value . time()) . '.png';
            // 2. Store the image on disk.
            File::put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path . '/' . $filename;

        }
    }
}
