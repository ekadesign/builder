<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Builder extends Model
{
    use CrudTrait;

    protected $fillable = [
        'logo',
        'img',
        'certification',
        'name',
        'body',
    ];

    protected $casts = [
        'certification' => 'array',
    ];

    /*
|--------------------------------------------------------------------------
| MUTATORS
|--------------------------------------------------------------------------
*/
    /**
     * @param $value
     */
    public function setLogoAttribute($value)
    {
        $attribute_name = "logo";
        $disk = "uploads";
        $destination_path = "logo";
        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});
            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value . time()) . '.png';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path . '/' . $filename;

        }
    }


    /**
     * @param $value
     */
    public function setImgAttribute($value)
    {
        $attribute_name = "img";
        $disk = "uploads";
        $destination_path = "img";
        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});
            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value . time()) . '.png';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path . '/' . $filename;

        }
    }


    public function setCertificationAttribute($value)
    {
        $attribute_name = "certification";
        $disk = "uploads";
        $destination_path = "certification";

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public function objects(){
        return $this->hasMany(Subject::class);
    }

    public function released_objects(){
        return $this->hasMany(Subject::class)->where('builded', 1);
    }
}
