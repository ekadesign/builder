<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use CrudTrait;

    protected $fillable = ['name'];

    public function subjects (){
        return $this->hasMany(Subject::class);
    }
}
