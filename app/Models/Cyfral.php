<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Cyfral extends Model
{
    use CrudTrait;

    protected $fillable = ['translit', 'value'];
}
