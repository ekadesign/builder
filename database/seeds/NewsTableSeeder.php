<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\News;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for ($i = 0; $i < 100; $i++){
            News::create(['name' => $faker->word, 'description' => $faker->text(400), 'tag' => $faker->text('30')]);
        }
    }
}
