<?php

use Illuminate\Database\Seeder;
use App\Models\ReleaseDate;

class ReleaseDatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReleaseDate::firstOrCreate(['release_date' => '1 квартал']);
        ReleaseDate::firstOrCreate(['release_date' => '2 квартал']);
        ReleaseDate::firstOrCreate(['release_date' => '3 квартал']);
        ReleaseDate::firstOrCreate(['release_date' => '4 квартал']);
    }
}
