<?php

use Illuminate\Database\Seeder;
use App\Models\Cyfral;

class CyfralsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cyfral::create(['name' => 'flat_builded', 'translit' => 'Квартир построено', 'value' => 0]);
        Cyfral::create(['name' => 'meters_builded', 'translit' => 'Кв. метров построено', 'value' => 0]);
        Cyfral::create(['name' => 'houses_in_sell', 'translit' => 'Домов в продаже', 'value' => 0]);
        Cyfral::create(['name' => 'flats_in_sell', 'translit' => 'Квартир в продаже', 'value' => 0]);

    }
}
