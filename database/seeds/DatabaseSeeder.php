<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CyfralsTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(BuildersTableSeeder::class);
        $this->call(JkTableSeeder::class);
        $this->call(ObjectsTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(ReleaseDatesTableSeeder::class);
    }
}
