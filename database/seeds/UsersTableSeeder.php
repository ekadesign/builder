<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use \Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrNew(['name' => 'eka', 'email' => 'ekadesign@bk.ru', 'password' => Hash::make('321668')]);
    }
}
