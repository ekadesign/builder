<?php

use Illuminate\Database\Seeder;
use App\Models\District;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        District::create(['name' => 'Фрунзенский']);
        District::create(['name' => 'Ленинский']);
        District::create(['name' => 'Октябрьский']);
        District::create(['name' => 'Веризино']);
    }
}
