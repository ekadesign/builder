<?php

use Illuminate\Database\Seeder;
use \App\Models\Jk;
use Faker\Factory;

class JkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for ($i = 0; $i < 20; $i++){
            Jk::create(['name' => $faker->country, 'district_id' => random_int(1, 4), 'builder_id' => random_int(1, 10)]);
        }
    }
}
