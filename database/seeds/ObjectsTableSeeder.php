<?php

use Illuminate\Database\Seeder;
use App\Models\Subject;
use Faker\Factory;

class ObjectsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Factory::create();
        for ($i = 0; $i < 300; $i++) {
            Subject::create([
                'name' => $faker->streetAddress,
                'district_id' => random_int(1, 4),
                'builder_id' => random_int(1, 10),
                'jk_id' => random_int(1, 20),
                'coordinates' => $faker->latitude(56.10, 56.16).','.$faker->longitude(40.37, 40.43)]);

        }
    }
}
