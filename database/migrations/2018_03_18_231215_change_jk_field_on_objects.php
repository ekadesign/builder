<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeJkFieldOnObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects', function (Blueprint $table){
            $table->dropColumn('jk');
            $table->integer('jk_id')->unsigned()->default(null);
            $table->foreign('jk_id')->references('id')->on('jks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subjects', function (Blueprint $table){
            $table->dropForeign(['jk_id']);
            $table->dropColumn('jk_id');
            $table->string('jk')->nullable();
        });
    }
}
