<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('builder_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->foreign('builder_id')->references('id')->on('builders');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jks', function (Blueprint $table){
            $table->dropForeign(['builder_id']);
            $table->dropForeign(['district_id']);
        });
        Schema::dropIfExists('jks');
    }
}
