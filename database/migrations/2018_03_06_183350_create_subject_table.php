<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->text('logo')->nullable();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('walls')->nullable();
            $table->string('level')->nullable();
            $table->string('heat')->nullable();
            $table->string('finish')->nullable();
            $table->text('body')->nullable();
            $table->integer('builder_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
