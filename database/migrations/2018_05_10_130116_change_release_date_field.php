<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeReleaseDateField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects', function (Blueprint $table){
            $table->renameColumn('release_date', 'release_date_id');
        });

        Schema::table('subjects', function (Blueprint $table) {
            $table->integer('release_date_id')->nullable()->unsigned()->change();
        });
        Schema::table('subjects', function (Blueprint $table) {
            $table->foreign('release_date_id')->references('id')->on('release_dates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->dropForeign(['release_date_id']);
        });
        Schema::table('subjects', function (Blueprint $table) {
            $table->renameColumn('release_date_id', 'release_date');
        });
        Schema::table('subjects', function (Blueprint $table) {
            $table->date('release_date')->nullable()->change();
        });
    }
}
