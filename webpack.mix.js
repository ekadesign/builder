let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .extract(['vue'])
   .sass('resources/assets/sass/app.scss', '../resources/assets/css/')
    .copyDirectory('resources/assets/img','public/img')
    .copyDirectory('resources/assets/js/scripts','public/js')
    .copyDirectory('resources/assets/fonts','public/fonts')
    .copyDirectory('resources/assets/css','public/css')
    .sourceMaps();


// mix.browserSync('developers.loc');

if (mix.inProduction()) {
    mix.version();
}
